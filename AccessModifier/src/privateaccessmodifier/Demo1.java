package privateaccessmodifier;



public class Demo1 {

	int a=10;
	String name="Poonam";
	
	private void Demo1() {
		
		System.out.println("private access modifier");
		
		
		
	}	
	
	public static void main(String[] args) {
		
		Demo1 d=new Demo1();
		System.out.println(d.a);
        System.out.println(d.name);
        
        d.Demo1();
        
        
        
		}
}
