package protectedaccesssmodifier;

public class Student1 {

	String name="Poonam";
	String name1="Payal";
	String name2="Manish";
	String name3="Pankaj";
	
	protected void Student1() {
		
		System.out.println("protected access modifier");
	}
	
	public static void main(String[] args) {
		
		Student1 s=new Student1();
		
		System.out.println(s.name);
		System.out.println(s.name1);
		System.out.println(s.name2);
		System.out.println(s.name3);
		
		s.Student1();
		
		
	}
	
	
}
