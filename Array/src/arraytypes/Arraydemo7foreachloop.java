package arraytypes;

public class Arraydemo7foreachloop {

	public static void main(String[] args) {
		
		
		int a[]=new int[6];
		a[0]=30;
		a[1]=50;
        a[2]=60;
        a[3]=70;
        a[4]=80;
        a[5]=20;
        
		for(int a2:a) {
		
			System.out.println(a2);
		
		}
		
	}
	
	
}
