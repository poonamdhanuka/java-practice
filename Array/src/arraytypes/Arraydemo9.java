package arraytypes;

public class Arraydemo9 {
 
	
	 public static void main(String[] args) {

			String s[] = new String[6];
			s[0] = "Manish";
			s[1] = "Payal";
			s[2] = "Pankaj";
			s[3] = "Poonam";
			s[4] = "Manju";
            s[5]="Prasad";
			for (int i = 0; i < 6; i++) {
				System.out.println(s[i]);
			}

			System.out.println("Array length is=" + s.length);

			for (int i = 0; i < s.length; i++) {
				System.out.println(s[i]);
			}

			System.out.println("By using for each loop ");

			for (String s3 : s) {
				System.out.println(s3);
			}

		
	

}
}
