package interface1;

public interface Test {

	int x=10;
}
     //public: To make it available for every implementation class.
        //static: Without existing object also we have to access this variable.
          //final: Implementation class can access this value but cannot modify.
        //  Hence inside interface the following declarations are  equal.
          // int x=10;
         //public int x=10;
          //static int x=10;
        //final int x=10; Equal
        //public static int x=10;
         //public final int x=10;
         //static final int x=10;
    //public static final int x=10;