package typesofoperator;

public class Logicaloperator {
	public static void main(String[] args) {
		System.out.println("Logical And");
		
		System.out.println((20>3)&&3>2);
		System.out.println((20>3&&3<2));
		System.out.println((20<3&&3>2));
		System.out.println((20<3)&&3<2);
		
		System.out.println("Logical OR");
		
		System.out.println((20>3||3>2));
		System.out.println((20>3||3<2));
		System.out.println((20<3||3>2));
		System.out.println((20<3||3<2));
		
		System.out.println("Logical Not");
		
		System.out.println((20>3));
		System.out.println(!( 20>3));
		
		
		
		
	}
	

}
