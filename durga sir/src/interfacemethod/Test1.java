package interfacemethod;

interface One{
		void methodOne();
		}
		class Two
		{
		public void methodTwo(){
		}
		}
		class Three extends Two implements One{
		public void methodOne(){
		}
		}
		//An interface can extend any no. Of interfaces at a time.
}
}