package methodoverriding;

import HasArelationship.child;

public class Child extends Parent {
	

public void show() {
		
		System.out.println("Child class show method");
	}
	
	public static void main(String[] args) {
		
		Parent p=new Parent();
		
		p.display();
		
		Child c=new Child();
		c.show();
		Parent p1=new Child();
		p1.display();
		
	//Child c=new Parent();	
	
}
}