package methodoverriding;

public class child  extends Parent{

	public void show() {
		
		System.out.println("Child class show method");
	}
	
	public static void main(String[] args) {
		
		Parent p=new Parent();
		
		p.display();
		
		child c=new child();
		c.show();
		Parent p1=new child();
		
		p1.display();
		
	//child c=new Parent();	
		
		
	}
	
}
