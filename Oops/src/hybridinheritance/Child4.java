package hybridinheritance;

public class Child4 extends Parent {

	public void m1() {
		
		System.out.println("method1 of class4");
		
	}
	
	public static void main(String[] args) {
		Child4 c4=new Child4();
		c4.add();
		c4.m1();
	}
}
