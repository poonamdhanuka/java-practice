package decisionmakingstatements;

public class Nestedifstatement {

	public static void main(String[] args) {
		int a = 89;
		int b = 24;
		int c = 54;

		if (a > b) {
			if (a > c) {
				System.out.println("Maximum no is=" + a);
			} else {
				System.out.println("Maximum no is=" + c);
			}
		} else {
			if (b > c) {
				System.out.println("Maximum no is=" + b);
			} else {
           System.out.println("Maximum no is="+c);
			}
		}
	}
}
